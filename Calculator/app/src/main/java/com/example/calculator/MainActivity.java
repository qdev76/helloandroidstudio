package com.example.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {

	EditText operand1;
	EditText operand2;
	Button add;
	Button subtract;
	Button divide;
	Button multiply;
	Button clear;
	TextView result;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //Operand fields on the main screen
        operand1 = (EditText) findViewById(R.id.editOperand1);
        operand2 = (EditText) findViewById(R.id.editOperand2);
        
        //Associate buttons
        add = (Button) findViewById(R.id.btnAdd);
        add.setOnClickListener(new OurOnClickListener(this));
        
        subtract = (Button) findViewById(R.id.btnSubtract);
        subtract.setOnClickListener(new OurOnClickListener(this));
        
        divide = (Button) findViewById(R.id.btnDivide);
        divide.setOnClickListener(new OurOnClickListener(this));
        
        multiply = (Button) findViewById(R.id.btnMultiply);
        multiply.setOnClickListener(new OurOnClickListener(this));
        
        clear = (Button) findViewById(R.id.btnClear);
        clear.setOnClickListener(new View.OnClickListener (){
        	
        	@Override
        	public void onClick(View view){
        		operand1.setText("");
        		operand2.setText("");
        		result.setText("0.00");
        	}
        });
        
        result = (TextView) findViewById(R.id.textView3);
        
        //Naive Approach
//        add.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				
//				//BigDecimal.setParseBigDecimal(true);
//				BigDecimal oper1 = BigDecimal.valueOf(Double.parseDouble(operand1.getText().toString()));
//				BigDecimal oper2 = BigDecimal.valueOf(Double.parseDouble(operand2.getText().toString()));
//		
//				result.setText(oper1.add(oper2).toString());
//				
//			}
//		});
        
//        subtract.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				
//				//BigDecimal.setParseBigDecimal(true);
//				BigDecimal oper1 = BigDecimal.valueOf(Double.parseDouble(operand1.getText().toString()));
//				BigDecimal oper2 = BigDecimal.valueOf(Double.parseDouble(operand2.getText().toString()));
//				
//				result.setText(oper1.subtract(oper2).toString());
//				
//			}
//		});
//        
//        divide.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				
//				//BigDecimal.setParseBigDecimal(true);
//				BigDecimal oper1 = BigDecimal.valueOf(Double.parseDouble(operand1.getText().toString()));
//				BigDecimal oper2 = BigDecimal.valueOf(Double.parseDouble(operand2.getText().toString()));
//				
//				result.setText(oper1.divide(oper2).toString());
//				
//			}
//		});
//
//		multiply.setOnClickListener(new View.OnClickListener() {
//		
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				
//				//BigDecimal.setParseBigDecimal(true);
//				BigDecimal oper1 = BigDecimal.valueOf(Double.parseDouble(operand1.getText().toString()));
//				BigDecimal oper2 = BigDecimal.valueOf(Double.parseDouble(operand2.getText().toString()));
//				
//				result.setText(oper1.multiply(oper2).toString());
//				
//			}
//		});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
