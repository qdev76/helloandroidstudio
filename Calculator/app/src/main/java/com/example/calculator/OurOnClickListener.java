package com.example.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class OurOnClickListener implements OnClickListener {
	MainActivity caller;
	
	public OurOnClickListener (MainActivity activity) {
		this.caller = activity;
	}
	
	public void onClick (View view)	{
		BigDecimal oper1 = BigDecimal.valueOf(Double.parseDouble(caller.operand1.getText().toString()));
		BigDecimal oper2 = BigDecimal.valueOf(Double.parseDouble(caller.operand2.getText().toString()));
		
		int opID = view.getId();
		
		switch (opID)
		{
			case R.id.btnAdd:
				caller.result.setText(oper1.add(oper2).toString());
				break;
			case R.id.btnSubtract:
				caller.result.setText(oper1.subtract(oper2).toString());
				break;
			case R.id.btnDivide:
				caller.result.setText(oper1.divide(oper2, 2, RoundingMode.HALF_UP).toString());
				break;
			case R.id.btnMultiply:
				caller.result.setText(oper1.multiply(oper2).toString());
				break;
		}
		
//		Toast msg = Toast.makeText(caller, "The view ID: " + opID, Toast.LENGTH_LONG);
//		msg.show();
//		msg = Toast.makeText(caller, "The subtract id: " + R.id.btnSubtract, Toast.LENGTH_LONG);
//		msg.show();
	}
}
